import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    // So far, we only have 1 (Feature) Module: the 'e-shop' one:
    { path: '', redirectTo: '/e-shop', pathMatch: 'full' }
    // Later on, we can have an Auth component/Module, inside 'core-modules'...
    // Or even a nice landing page @ this 'src\app\app.component.html'
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
