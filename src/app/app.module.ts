import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// REDUX affairs:
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// .... and some fancy options:
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { environment } from '../environments/environment';

// MODULES:
import { EShopModule } from 'src/app/features-modules/e-shop/e-shop.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,

        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),

        // --------------------------
        StoreDevtoolsModule.instrument({ maxAge: 15 }),
        // Stuff Angular's routes into the App's Store:
        StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
        // To use Chrome browser extension called Redux DevTools:
        !environment.production ? StoreDevtoolsModule.instrument() : [],
        // --------------------------

         // ... and the FEATURES Modules:
        EShopModule,

        // set order of AppRoutingModule to be the LAST after above-added modules.
        AppRoutingModule
    ],
    providers: [],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
