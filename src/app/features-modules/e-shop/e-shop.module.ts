import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

// REDUX affairs:
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// ... and to use the redux-store of this Feature Module 'e-shop':
import { featureShopReducerName, featureShopReducers } from './redux-store/store-pack.reducers';
import { ProductsEffects } from './redux-store/effects/products.effects';

import { EShopRoutingModule } from './e-shop-routing.module';

import { ProductsListComponent } from './components/products-list/products-list.component';
import { ShoppingCartIconComponent } from './components/products-list/shopping-cart-icon/shopping-cart-icon.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';


@NgModule({
    declarations: [
        ProductsListComponent,
        ShoppingCartIconComponent,
        ShoppingCartComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,

        StoreModule.forFeature(featureShopReducerName, featureShopReducers),
        EffectsModule.forFeature([ProductsEffects]),


        // set order of RoutingModule to be the LAST after above-added modules:
        EShopRoutingModule

    ],
    exports: [
        ShoppingCartIconComponent    // <= is used @ 'src\app\app.component.html'
    ],
    providers: []
})
export class EShopModule { }
