/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { EShopComponent } from './e-shop.component';

describe('EShopComponent', () => {
  let component: EShopComponent;
  let fixture: ComponentFixture<EShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
