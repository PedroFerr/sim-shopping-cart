
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { Store, select } from '@ngrx/store';
import { IeShopState, IShoppingCartState } from '../redux-store/store-pack.state.interfaces';
import { selectShoppingCart } from '../redux-store/store-pack.selectors';


@Injectable({
    providedIn: 'root',
})
export class ShoppingCartContentService implements Resolve<IShoppingCartState> {

    shoppingCart$: Observable<IShoppingCartState>;

    constructor(
        private eShopStoreSlice: Store<IeShopState>,
    ) {  }

    // tslint:disable-next-line: max-line-length
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IShoppingCartState> | Promise<IShoppingCartState> | IShoppingCartState {

        this.shoppingCart$ = this.eShopStoreSlice.pipe(select(selectShoppingCart));

        // return this.shoppingCart$;
        // We need to complete the Observable, otherwise Resolver will never return from here - routed URL will never be achieved:
        return this.shoppingCart$.pipe(take(1));
        // Meaning, in fact, the Angular Router already subscribes to the Observable,
        // returning, on the resolved routed component, a scalar value - NOT an Observable.
        // (this ShoppingCartContentService Class implements 'Resolve<IShoppingCartState>')
    }
}
