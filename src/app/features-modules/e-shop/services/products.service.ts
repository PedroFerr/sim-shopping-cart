import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Product } from '../e-shop.interfaces';


@Injectable({
    providedIn: 'root'
})
export class ProductsService {

    productsAPI = `https://5ee744ce52bb0500161fd6e4.mockapi.io/products`;

    constructor(private httpCall: HttpClient) { }

    getProducts(): Observable<Array<Product>> {
        return this.httpCall.get<Array<Product>>(this.productsAPI);
    }
}
