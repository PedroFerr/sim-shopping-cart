import { Component, OnInit, Input, ElementRef, Renderer2, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Data } from '@angular/router';
import { Store } from '@ngrx/store';

import { Observable, Subscription } from 'rxjs';

// Redux Store slice:
import { IeShopState, IShoppingCartState, IProductsState } from '../../redux-store/store-pack.state.interfaces';
import { AddItem, RemoveItem } from '../../redux-store/actions/shoppingCart.actions';

import { ShoppingCartProduct } from '../../e-shop.interfaces';

@Component({
    selector: 'app-shopping-cart',
    templateUrl: './shopping-cart.component.html',
    styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit, OnDestroy {

    componentDOM: HTMLElement;

    routedDataSubscription: Subscription;
    routedShoppingCart: IShoppingCartState;

    @Input() inputtedCartContent: IShoppingCartState;

    hoveredProductPhoto: string;

    constructor(
        private hostElement: ElementRef,
        private ngRenderer: Renderer2,

        private router: Router,
        private activatedRoute: ActivatedRoute,

        private eShopStoreSlice: Store<IeShopState>
    ) { }

    ngOnInit() {
        this.getRoutedShopingCart();
        this.componentDOM = this.hostElement.nativeElement;
    }

    ngOnDestroy() {
        // In fact we do NOT need to unsubscribe() - Angular Router takes care of that. But, it's always a good practice:
        if (this.routedDataSubscription) {
            this.routedDataSubscription.unsubscribe();
        }
    }

    onMouseEnteringCartProductLine(ofProduct: ShoppingCartProduct) {
        const productImgContainer = this.componentDOM.querySelector('.cart-line-image-container');
        this.ngRenderer.removeClass(productImgContainer, 'hidden');
        this.hoveredProductPhoto = ofProduct.photo;
    }

    onMouseLeavingCartProductLine() {
        const productImgContainer = this.componentDOM.querySelector('.cart-line-image-container');
        this.ngRenderer.addClass(productImgContainer, 'hidden');
        this.hoveredProductPhoto = null;
    }

    incrementQty(ofProductId: string) {
        // 'shoppingCartReducers' will know if it's a new item, or an existent one - incrementing 'qty'
        this.eShopStoreSlice.dispatch(new AddItem(ofProductId));

        // And now for the current HTML rendering:
        const aPsTemp = [] as Array<ShoppingCartProduct>;
        let newNproducts = 0;
        let newTotalCost = 0;
        this.routedShoppingCart.cartProducts.forEach((cartP: ShoppingCartProduct) => {
            if (cartP.id === ofProductId) {
                cartP = Object.assign({}, cartP, {qty: cartP.qty + 1});
            }
            aPsTemp.push(cartP);
            newNproducts = newNproducts + cartP.qty;
            newTotalCost = newTotalCost + (cartP.qty * cartP.price);
        });
        // And so:
        this.routedShoppingCart = {
            cartProducts: aPsTemp,
            nProducts: newNproducts,
            nTotalCost: newTotalCost
        };
    }

    decrementQty(ofProductId: string) {
        const cartProductToRemove: ShoppingCartProduct = this.routedShoppingCart.cartProducts.filter(
            (p: ShoppingCartProduct) => p.id === ofProductId
        )[0];
        // Check if current Qty = 1; if so.... remove it for good:
        const confirmMsg = 'Are you sure you want to REMOVE\nthis item from the shopping cart...?';
        const hasUniqueQty: boolean = cartProductToRemove.qty === 1;

        // Here we start by preparing current, that might be different, HTML rendering:
        const aPsTemp = [] as Array<ShoppingCartProduct>;
        let newNproducts = 0;
        let newTotalCost = 0;

        this.routedShoppingCart.cartProducts.forEach((cartP: ShoppingCartProduct) => {
            if (cartP.id !== ofProductId) {
                aPsTemp.push(cartP);
                newNproducts = newNproducts + cartP.qty;
                newTotalCost = newTotalCost + (cartP.qty * cartP.price);
            } else {
                if (!hasUniqueQty) {
                    // Count it in, but decrementing Qty:
                    cartP = Object.assign({}, cartP, {qty: cartP.qty - 1});
                    aPsTemp.push(cartP);
                    newNproducts = newNproducts + cartP.qty;
                    newTotalCost = newTotalCost + (cartP.qty * cartP.price);
                } else {
                    // It's really to remove => don't push it
                }
            }
        });
        // And so:
        const userIsConfirmingRemoval: boolean = hasUniqueQty ? window.confirm(confirmMsg) : null;
        if (userIsConfirmingRemoval || !hasUniqueQty) {

            this.eShopStoreSlice.dispatch(new RemoveItem(ofProductId));

            this.routedShoppingCart = {
                cartProducts: aPsTemp,
                nProducts: newNproducts,
                nTotalCost: newTotalCost
            };
        }
    }


    navigateBackToProducts() {
        this.closeShoppingCartIconDropdown();
        this.router.navigateByUrl('/e-shop');
    }

    navigateToShoppingCartPage() {
        this.closeShoppingCartIconDropdown();
        this.router.navigateByUrl('/shopping-cart');
    }

    private getRoutedShopingCart() {
        const routedData$: Observable<Data> = this.activatedRoute.data;
        this.routedDataSubscription = routedData$.subscribe(
            (routedResolverData: {shoppingCart: IShoppingCartState} ) => this.routedShoppingCart = routedResolverData.shoppingCart
        );
    }

    private closeShoppingCartIconDropdown() {
        document.querySelector('.shopping-cart-content').classList.add('hidden');
    }
}
