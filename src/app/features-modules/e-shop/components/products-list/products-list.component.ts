import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { Observable, Subscription } from 'rxjs';

// Redux Store slice:
import { IeShopState, IProductsState } from '../../redux-store/store-pack.state.interfaces';
// Shop Products:
import { GetProducts } from '../../redux-store/actions/products.actions';
import { selectProducts } from '../../redux-store/store-pack.selectors';
// Shopping Cart Products:
import { AddItem } from '../../redux-store/actions/shoppingCart.actions';

@Component({
    selector: 'app-products-list',
    templateUrl: './products-list.component.html',
    styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit, OnDestroy {

    shopProducts$: Observable<IProductsState>;
    shopProductsSubscription: Subscription;

    constructor(
        private eShopStoreSlice: Store<IeShopState>
    ) { }

    ngOnInit() {
        // Trigger the Action to fetch over our API:
        this.eShopStoreSlice.dispatch(new GetProducts());
        // Start listening for the http fetch results:
        this.shopProducts$ = this.eShopStoreSlice.pipe(select(selectProducts));
    }

    ngOnDestroy() {
        if (this.shopProductsSubscription) {
            this.shopProductsSubscription.unsubscribe();
        }
    }

    addToCart(productId: string) {
        this.eShopStoreSlice.dispatch(new AddItem(productId));
    }

}
