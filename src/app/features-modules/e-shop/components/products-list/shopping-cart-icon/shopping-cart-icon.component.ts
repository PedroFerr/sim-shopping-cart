import { Component, OnInit, OnDestroy, ElementRef, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';

import { Store, select } from '@ngrx/store';

import { Observable, Subscription } from 'rxjs';

// Redux Store slice:
import { IeShopState, IShoppingCartState } from '../../../redux-store/store-pack.state.interfaces';
// Shopping Cart Products:
import { selectShoppingCart } from '../../../redux-store/store-pack.selectors';


@Component({
    selector: 'app-shopping-cart-icon',
    templateUrl: './shopping-cart-icon.component.html',
    styleUrls: ['./shopping-cart-icon.component.scss']
})
export class ShoppingCartIconComponent implements OnInit, OnDestroy {

    componentDOM: HTMLElement;
    shopCartContent: HTMLElement;

    shoppingCartState$: Observable<IShoppingCartState>;
    shoppingCartStateSubscription: Subscription;

    constructor(
        private hostElement: ElementRef,
        private ngRenderer: Renderer2,

        private eShopStoreSlice: Store<IeShopState>,

        private router: Router
    ) { }

    ngOnInit() {
        this.componentDOM = this.hostElement.nativeElement;
        this.shopCartContent = this.componentDOM.querySelector('.shopping-cart-content');

        this.shoppingCartState$ = this.eShopStoreSlice.pipe(select(selectShoppingCart));
    }

    ngOnDestroy() {
        if (this.shoppingCartStateSubscription) {
            this.shoppingCartStateSubscription.unsubscribe();
        }
    }

    onMouseEnteringCartIcon() {
        this.ngRenderer.removeClass(this.shopCartContent, 'hidden');
    }

    onMouseLeavingCartContent() {
        this.ngRenderer.addClass(this.shopCartContent, 'hidden');
    }

    navigateToShoppingCartPage() {
        this.onMouseLeavingCartContent();
        this.router.navigateByUrl('/shopping-cart');
    }
}
