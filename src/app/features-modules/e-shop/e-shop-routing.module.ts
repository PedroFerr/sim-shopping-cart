import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsListComponent } from './components/products-list/products-list.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';

// Our Router Resolver, to have the Shopping CArt content ANYWHERE, in any Routing, before component mounts:
import { ShoppingCartContentService } from './services/shopping-cart.resolver';

export const routes: Routes = [
    // We could go to the presentation component of this Feature MOdule:
    // { path: 'e-shop', component: EShopComponent },
    //
    // But let's go directly to the Products listing page, making this the landing page of this 'e-shop' Feature Module:
    { path: 'e-shop', component: ProductsListComponent },
    // { path: 'shopping-cart', component: ShoppingCartComponent },
    { path: 'shopping-cart', component: ShoppingCartComponent, resolve: { shoppingCart: ShoppingCartContentService } },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class EShopRoutingModule { }
