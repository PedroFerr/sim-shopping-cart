
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { Effect, ofType, Actions } from '@ngrx/effects';
import { Observable, of, forkJoin, combineLatest } from 'rxjs';
import { switchMap, map, withLatestFrom, tap, finalize, catchError, mapTo, concatMapTo, concatMap, filter, take } from 'rxjs/operators';

import { Store } from '@ngrx/store';

import {
    EProductsActions, ProductsActions,
    GetProducts, GetProductsSuccess, GetProductsError
} from '../actions/products.actions';
import { selectProducts } from '../store-pack.selectors';
import { IProductsState, IeShopState } from '../store-pack.state.interfaces';

import { Product } from '../../e-shop.interfaces';

import { ProductsService } from '../../services/products.service';

@Injectable()
export class ProductsEffects {

    constructor(
        private eShopStoreSlice: Store<IeShopState>,
        private productsService: ProductsService,
        private actions$: Actions
    ) { }

    @Effect()
    getProducts$: Observable<ProductsActions> = this.actions$.pipe(
        ofType<GetProducts>(EProductsActions.GET_PRODUCTS),

        // We shon't call for the http service if the Shop Products are already in the Store, from some previous State.
        // So we must sniff first what have we got already in the Store, on this precise State:

        // switchMap(() => this.productsService.getProducts()),
        // switchMap(
        //     (products: Array<Product>) => of(new GetProductsSuccess(products))
        // )
    )
    .pipe(
        map((action: GetProducts) => action.type), // <= no payload, to successfuly request the service.
        withLatestFrom(this.eShopStoreSlice.select(selectProducts)),

        switchMap(([payload, stateProducts]: [string, IProductsState]) => {
            if (!stateProducts || stateProducts.shopProducts.length === 0) {
                return this.productsService.getProducts()
                    .pipe(
                        map(
                            (products: Array<Product>) => new GetProductsSuccess(products)
                        ),
                        catchError((error: HttpErrorResponse) => of(new GetProductsError(error)))
                    )
                    ;
            }
            // If we reach here, there was no need to htpp fetch - just payload with what's in the Store, current State:
            return of(new GetProductsSuccess(stateProducts.shopProducts));
        })
    );

    // =============================================

    // Finally, but not the least:
    @Effect({ dispatch: false })
    getProductsFailed$: Observable<GetProductsError> = this.actions$.pipe(
        ofType<GetProductsError>(EProductsActions.GET_PRODUCTS_ERROR),
        tap((payload: GetProductsError) => {
            // You could here trigger some Actions, that could trigger other Effects/Services, and so on:
            // this.eShopStoreSlice.dispatch(new loadingActions.TurnLoadingOff());
            // this.eShopStoreSlice.dispatch(new errorActions.ErrorLayoutShow(payload));

            // console.error('Redux Store ERROR for "getProductsFailed$" Effect:', payload);
            // this.eShopStoreSlice.dispatch(new RemoveProducts());

            // TODO: would be nice, if things escalate, to have a "common" error...?
            // console.error('Redux Store ERROR for e-shop "getFeatureState" Selectors of IeShopState:', payload);
        })
    );

}
