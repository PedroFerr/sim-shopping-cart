import { Action } from '@ngrx/store';

import { ShoppingCartProduct } from '../../e-shop.interfaces';

export enum EShoppingCartActions {
    GET_SHOPPING_CART_PRODUCTS = '[e-shop Shopping Cart] Get Products',
    GET_SHOPPING_CART_PRODUCTS_SUCCESS = '[e-shop Shopping Cart] Get Products SUCCESS',
    ADD_ITEM = '[e-shop Shopping Cart] Add Product',
    REMOVE_ITEM = '[e-shop Shopping Cart] Remove Product'
}

export class GetShoppingCartProducts implements Action {
    public readonly type = EShoppingCartActions.GET_SHOPPING_CART_PRODUCTS;
}

export class GetShoppingCartProductsSuccess implements Action {
    public readonly type = EShoppingCartActions.GET_SHOPPING_CART_PRODUCTS_SUCCESS;
    constructor(public payload: Array<ShoppingCartProduct>) { }
}

export class AddItem implements Action {
    public readonly type = EShoppingCartActions.ADD_ITEM;
    constructor(public payload: string) {
        // It's a number stringified, like "2"
    }
}

export class RemoveItem implements Action {
    public readonly type = EShoppingCartActions.REMOVE_ITEM;
    constructor(public payload: string) {
        // It's a number stringified, like "2"
    }
}



export type ShoppingCartActions =
    | GetShoppingCartProducts
    | GetShoppingCartProductsSuccess
    | AddItem
    | RemoveItem
;

