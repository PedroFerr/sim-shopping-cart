import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';

import { Product } from '../../e-shop.interfaces';

export enum EProductsActions {
    GET_PRODUCTS = '[e-shop Products] Get Products',
    GET_PRODUCTS_SUCCESS = '[e-shop Products] Get Products SUCCESS',
    GET_PRODUCTS_ERROR = '[e-shop Products] ERROR on getting Products!',

}

export class GetProducts implements Action {
    public readonly type = EProductsActions.GET_PRODUCTS;
}

export class GetProductsSuccess implements Action {
    public readonly type = EProductsActions.GET_PRODUCTS_SUCCESS;
    constructor(public payload: Array<Product>) { }
}

export class GetProductsError implements Action {
    readonly type = EProductsActions.GET_PRODUCTS_ERROR;
    constructor(public payload: HttpErrorResponse) { }
}


export type ProductsActions =
    | GetProducts
    | GetProductsSuccess
    | GetProductsError
;

