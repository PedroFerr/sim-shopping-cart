import { createSelector, createFeatureSelector } from '@ngrx/store';

// Our 'e-shop' Feature Module Redux STATE interfaces:
import { IeShopState} from './store-pack.state.interfaces';
import { featureShopReducerName } from './store-pack.reducers';

const getFeatureState = createFeatureSelector<IeShopState>(featureShopReducerName);

// And the SELECTORs, for each different STATE, that this Feature Module can operate in the entire App:
export const selectProducts = createSelector( getFeatureState, (state: IeShopState) => state.products);
export const selectShoppingCart = createSelector( getFeatureState, (state: IeShopState) => state.shoppingCart);
