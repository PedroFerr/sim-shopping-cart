import { IShoppingCartState, initialShopState } from '../store-pack.state.interfaces';
import { ShoppingCartActions, EShoppingCartActions } from '../actions/shoppingCart.actions';

import { currentShopProducts } from './products.reducers';

import { Product, ShoppingCartProduct, ProductPropertyToChoose } from '../../e-shop.interfaces';


export const shoppingCartReducers = (state = initialShopState.shoppingCart, action: ShoppingCartActions): IShoppingCartState => {

    switch (action.type) {
        case EShoppingCartActions.GET_SHOPPING_CART_PRODUCTS_SUCCESS: {
            return {
                ...state,
                cartProducts: action.payload
            };
        }

        case EShoppingCartActions.ADD_ITEM: {
            const productId: string = action.payload;
            const shopProductToAdd: Product = currentShopProducts.filter((p: Product) => p.id === productId)[0];

            // For now:
            let currentNumberItems = state.nProducts;
            let totalItemsPrice: number = state.nTotalCost;
            let modifiedCart: Array<ShoppingCartProduct> = state.cartProducts;
            // And that's what we want to get ride of and enter into a new State, coming here.

            if (shopProductToAdd) {
                currentNumberItems ++;
                modifiedCart = addShopProductIntoNewArray(shopProductToAdd, state.cartProducts);
                totalItemsPrice = 0;
                modifiedCart.forEach((p: ShoppingCartProduct) => {
                    totalItemsPrice = totalItemsPrice + (p.qty * p.price);
                });
            }

            return {
                ...state,
                cartProducts: modifiedCart,
                nProducts: currentNumberItems,
                nTotalCost: totalItemsPrice
            };

        }

        case EShoppingCartActions.REMOVE_ITEM: {
            const productId: string = action.payload;
            const cartProductToRemove: ShoppingCartProduct = state.cartProducts.filter((p: ShoppingCartProduct) => p.id === productId)[0];

            let currentNumberItems = state.nProducts;
            let totalItemsPrice: number = state.nTotalCost;
            let modifiedCart: Array<ShoppingCartProduct> = state.cartProducts;

            if (cartProductToRemove) {
                modifiedCart = removeCartProductFromArray(cartProductToRemove, state.cartProducts);
                currentNumberItems --;
                totalItemsPrice = state.nTotalCost - cartProductToRemove.price;
            }

            return {
                ...state,
                cartProducts: modifiedCart,
                nProducts: currentNumberItems,
                nTotalCost: totalItemsPrice
            };

        }

        default:
            return state;
    }
};


const removeCartProductFromArray = (shopCartP: ShoppingCartProduct, cartPs: Array<ShoppingCartProduct>): Array<ShoppingCartProduct> => {
    const changedArray = [] as Array<ShoppingCartProduct>;
    const hasUniqueQty: boolean = shopCartP.qty === 1;

    for (let cartP of cartPs) {
        if (cartP.id !== shopCartP.id) {
            changedArray.push(cartP);
        } else {
            if (!hasUniqueQty) {
                // Count it in, but decrementing Qty:
                cartP = Object.assign({}, cartP, {qty: cartP.qty - 1});
                changedArray.push(cartP);
            } else {
                // It's really to remove => don't push it
            }
        }
    }
    return changedArray;
};

/**
 * We have o analyse if the 'shopP' item/Product to add
 * is already in the Shopping Cart or not
 *
 * @param shopP - the Shop Product chosen/clicked by the User
 * @param cartPs - the entire Array of Products currently existent on our Shopping Cart
 * @returns - the, now changed (either by Qty on an existent item, or by adding a new one), Array of Shopping Cart products
 */
const addShopProductIntoNewArray = (shopP: Product, cartPs: Array<ShoppingCartProduct>): Array<ShoppingCartProduct> => {
    const pExistsOnCart = cartPs.filter(p => p.id === shopP.id).length === 1;
    return pExistsOnCart ? changeQtyCartProduct(shopP, cartPs) : newCartProduct(shopP, cartPs);
};

/**
 * This 'shopP' is already on current Shopping Cart.
 * Just increment its quantity.
 *
 * @param shopP - the Shop Product chosen/clicked by the User
 * @param cartPs - the entire Array of Products currently existent on our Shopping Cart
 * @returns - the, now changed by Qty, Array of Shopping Cart products
 */

const changeQtyCartProduct = (shopP: Product, cartPs: Array<ShoppingCartProduct>): Array<ShoppingCartProduct> => {
    const changedArray = [] as Array<ShoppingCartProduct>;

    for (let cartP of cartPs) {
        if (cartP.id === shopP.id) {
            cartP = Object.assign({}, cartP, {qty: cartP.qty + 1});
        }
        changedArray.push(cartP);
    }
    return changedArray;
};

/**
 * A new Product is to be added to current Shopping Cart
 *
 * @param shopP - the Shop Product chosen/clicked by the User
 * @param cartPs - the entire Array of Products currently existent on our Shopping Cart
 * @returns - the, now changed by a new item, Array of Shopping Cart products
 */
const newCartProduct = (shopP: Product, cartPs: Array<ShoppingCartProduct>): Array<ShoppingCartProduct> => {
    const newProduct: ShoppingCartProduct = Object.assign({}, shopP, {
        qty: 1,
        color: chosenProp(shopP.color),
        size: chosenProp(shopP.size)
    });
    return [...cartPs, ...[newProduct]];
};

/**
 * We are here sort of extending the ShoppingCartProduct.
 *
 * Most of times, on an e-shop User has to select, before adding the Shop Product to Cart,
 * some properties of the Product: i.e. 'color' and/or 'size' (or none - if Product has no variances...)
 * Last is the current case: on current 'src\app\features-modules\e-shop\services\products.service.ts' end point,
 * all the coming items/Products don't have any specific properties for User to choose.
 *
 * @param shopPprops - the options, for User to select one, on any pre-defined property
 * @returns - the scalar property value, chosen by the User - a string
 */
const chosenProp = (shopPprops: Array<ProductPropertyToChoose>): string => {
    let valueToReturn: string;
    const defaultValueToReturn = 'n/e';

    valueToReturn = defaultValueToReturn;

    if (shopPprops) {
        const chosenElem: ProductPropertyToChoose = shopPprops.filter((propToChoose: ProductPropertyToChoose) => propToChoose.chosen)[0];
        if (chosenElem) {
            valueToReturn = chosenElem.propId;
        }
    }

    return valueToReturn;
};
