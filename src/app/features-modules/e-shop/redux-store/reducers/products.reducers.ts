import { IProductsState, initialShopState } from '../store-pack.state.interfaces';
import { ProductsActions, EProductsActions } from '../actions/products.actions';

import { Product } from '../../e-shop.interfaces';

// Used on other Reducers.... This is, in fact, our source, our e-shiop:
export const currentShopProducts = [] as Array<Product>;

export const productsReducers = (state = initialShopState.products, action: ProductsActions): IProductsState => {

    switch (action.type) {
        case EProductsActions.GET_PRODUCTS_SUCCESS: {
            const fetchedProducts = action.payload;
            currentShopProducts.length = 0;
            fetchedProducts.forEach(p => currentShopProducts.push(p));

            return {
                ...state,
                shopProducts: fetchedProducts
            };
        }

        case EProductsActions.GET_PRODUCTS_ERROR: {
            return {
                ...state,
                shopProducts: null,
                errorMessage: action.payload,
            };
        }

        default:
            return state;
    }
};
