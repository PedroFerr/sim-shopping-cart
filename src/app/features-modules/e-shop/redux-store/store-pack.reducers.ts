import { ActionReducerMap } from '@ngrx/store';

import { IeShopState } from './store-pack.state.interfaces';

import { productsReducers } from './reducers/products.reducers';
import { shoppingCartReducers } from './reducers/shoppingCart.reducers';

export const featureShopReducerName = 'e-shop';
export const featureShopReducers: ActionReducerMap<IeShopState, any> = {
    products: productsReducers,
    shoppingCart: shoppingCartReducers
};
