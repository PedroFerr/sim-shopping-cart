import { HttpErrorResponse } from '@angular/common/http';

import { Product, ShoppingCartProduct } from '../e-shop.interfaces';

export interface IeShopState {
    products: IProductsState;
    shoppingCart: IShoppingCartState;
}
export const initialShopState: IeShopState = {
    products: null,
    shoppingCart: {
        cartProducts: [],
        nProducts: 0,
        nTotalCost: 0
    }
};

export interface IProductsState {
    shopProducts: Array<Product>;
    errorMessage?: HttpErrorResponse;
}

export interface IShoppingCartState {
    cartProducts: Array<ShoppingCartProduct>;
    nProducts: number;
    nTotalCost: number;
}




