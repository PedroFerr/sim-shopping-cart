export interface Product {
    id: string;
    name: string;
    price: number;
    photo: string;
    SKU?: string;
    color?: Array<ProductPropertyToChoose>;
    size?: Array<ProductPropertyToChoose>;
}

export interface ShoppingCartProduct {
    id: string;
    name: string;
    price: number;
    photo: string;
    qty: number;
    color?: string;
    size?: string;
}

export interface ProductPropertyToChoose {
    chosen: boolean;
    propId: string;
}
